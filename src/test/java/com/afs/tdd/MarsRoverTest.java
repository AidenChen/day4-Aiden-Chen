package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {

    @Test
    void show_given_location_0_0_N_command_M_when_executeBatchCommands_then_location_0_1_N() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Move));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(1, marsRover.getLocation().getY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_N_command_Left_When_executeBatchCommands_then_location_0_0_W() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Left));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_N_command_Right_When_executeBatchCommands_then_location_0_0_E() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Right));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_S_command_M_When_executeBatchCommands_then_location_0_f1_S() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Move));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(-1, marsRover.getLocation().getY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_S_command_Left_When_executeBatchCommands_then_location_0_0_E() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Left));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_S_command_Right_When_executeBatchCommands_then_location_0_0_W() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Right));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_E_command_M_When_executeBatchCommands_then_location_1_0_E() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Move));
        //then
        assertEquals(1, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_E_command_Left_When_executeBatchCommands_then_location_0_0_N() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Left));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_E_command_Right_When_executeBatchCommands_then_location_0_0_S() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Right));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_W_command_M_When_executeBatchCommands_then_location_f1_0_W() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Move));
        //then
        assertEquals(-1, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_location_0_0_W_command_Left_When_executeBatchCommands_then_location_0_0_S() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Left));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void
    show_given_location_0_0_W_command_Right_When_executeBatchCommands_then_location_0_0_N() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Right));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void show_given_list_location_0_0_W_command_Right_When_executeBatchCommands_then_location_0_0_N() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeBatchCommands(Collections.singletonList(Command.Right));
        //then
        assertEquals(0, marsRover.getLocation().getX());
        assertEquals(0, marsRover.getLocation().getY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }
}
