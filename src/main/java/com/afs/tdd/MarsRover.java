package com.afs.tdd;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class MarsRover {

    private final Direction[] directions = {Direction.North, Direction.West, Direction.South, Direction.East};

    private final int minLimit = 0;  // directions min index
    private final int maxLimit = 3;  // direction max index
    private int curDirectionIndex;

    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
        this.curDirectionIndex = Arrays.asList(directions).indexOf(location.getDirection());
    }

    public void executeCommand(Command command) {
        if (Command.Move.equals(command)) {
            move();
        }
        if (Command.Left.equals(command)) {
            left();
        }
        if (Command.Right.equals(command)) {
            right();
        }
    }

    public void executeBatchCommands(List<Command> commands) {
        commands.forEach(this::executeCommand);
    }

    private void right() {
        curDirectionIndex = curDirectionIndex == minLimit ? maxLimit : curDirectionIndex - 1;
        location.setDirection(directions[curDirectionIndex]);
    }

    private void left() {
        curDirectionIndex = curDirectionIndex == maxLimit ? minLimit : curDirectionIndex + 1;
        location.setDirection(directions[curDirectionIndex++]);
    }

    private void move() {
        switch (location.getDirection()) {
            case East:
                location.setX(location.getX() + 1);
                break;
            case South:
                location.setY(location.getY() - 1);
                break;
            case North:
                location.setY(location.getY() + 1);
                break;
            case West:
                location.setX(location.getX() - 1);
                break;
        }
    }

    public Location getLocation() {
        return location;
    }
}
